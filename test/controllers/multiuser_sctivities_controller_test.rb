require 'test_helper'

class MultiuserActivitiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @multiuser_sctivity = multiuser_activities(:one)
  end

  test "should get index" do
    get multiuser_activities_url, as: :json
    assert_response :success
  end

  test "should create multiuser_sctivity" do
    assert_difference('MultiuserSctivity.count') do
      post multiuser_activities_url, params: { multiuser_sctivity: { activity_id: @multiuser_sctivity.activity_id, multiuser_id: @multiuser_sctivity.multiuser_id, year: @multiuser_sctivity.year } }, as: :json
    end

    assert_response 201
  end

  test "should show multiuser_sctivity" do
    get multiuser_sctivity_url(@multiuser_sctivity), as: :json
    assert_response :success
  end

  test "should update multiuser_sctivity" do
    patch multiuser_sctivity_url(@multiuser_sctivity), params: { multiuser_sctivity: { activity_id: @multiuser_sctivity.activity_id, multiuser_id: @multiuser_sctivity.multiuser_id, year: @multiuser_sctivity.year } }, as: :json
    assert_response 200
  end

  test "should destroy multiuser_sctivity" do
    assert_difference('MultiuserSctivity.count', -1) do
      delete multiuser_sctivity_url(@multiuser_sctivity), as: :json
    end

    assert_response 204
  end
end
