require 'test_helper'

class QualisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @quali = qualis(:one)
  end

  test "should get index" do
    get qualis_url, as: :json
    assert_response :success
  end

  test "should create quali" do
    assert_difference('Quali.count') do
      post qualis_url, params: { quali: { tier: @quali.tier } }, as: :json
    end

    assert_response 201
  end

  test "should show quali" do
    get quali_url(@quali), as: :json
    assert_response :success
  end

  test "should update quali" do
    patch quali_url(@quali), params: { quali: { tier: @quali.tier } }, as: :json
    assert_response 200
  end

  test "should destroy quali" do
    assert_difference('Quali.count', -1) do
      delete quali_url(@quali), as: :json
    end

    assert_response 204
  end
end
