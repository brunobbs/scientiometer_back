require 'test_helper'

class ParticipationTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @participation_type = participation_types(:one)
  end

  test "should get index" do
    get participation_types_url, as: :json
    assert_response :success
  end

  test "should create participation_type" do
    assert_difference('ParticipationType.count') do
      post participation_types_url, params: { participation_type: { corresponding_author: @participation_type.corresponding_author, first_author: @participation_type.first_author, last_author: @participation_type.last_author } }, as: :json
    end

    assert_response 201
  end

  test "should show participation_type" do
    get participation_type_url(@participation_type), as: :json
    assert_response :success
  end

  test "should update participation_type" do
    patch participation_type_url(@participation_type), params: { participation_type: { corresponding_author: @participation_type.corresponding_author, first_author: @participation_type.first_author, last_author: @participation_type.last_author } }, as: :json
    assert_response 200
  end

  test "should destroy participation_type" do
    assert_difference('ParticipationType.count', -1) do
      delete participation_type_url(@participation_type), as: :json
    end

    assert_response 204
  end
end
