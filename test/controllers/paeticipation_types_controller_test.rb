require 'test_helper'

class PaeticipationTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @paeticipation_type = paeticipation_types(:one)
  end

  test "should get index" do
    get paeticipation_types_url, as: :json
    assert_response :success
  end

  test "should create paeticipation_type" do
    assert_difference('PaeticipationType.count') do
      post paeticipation_types_url, params: { paeticipation_type: { corresponding_author: @paeticipation_type.corresponding_author, first_author: @paeticipation_type.first_author, last_author: @paeticipation_type.last_author } }, as: :json
    end

    assert_response 201
  end

  test "should show paeticipation_type" do
    get paeticipation_type_url(@paeticipation_type), as: :json
    assert_response :success
  end

  test "should update paeticipation_type" do
    patch paeticipation_type_url(@paeticipation_type), params: { paeticipation_type: { corresponding_author: @paeticipation_type.corresponding_author, first_author: @paeticipation_type.first_author, last_author: @paeticipation_type.last_author } }, as: :json
    assert_response 200
  end

  test "should destroy paeticipation_type" do
    assert_difference('PaeticipationType.count', -1) do
      delete paeticipation_type_url(@paeticipation_type), as: :json
    end

    assert_response 204
  end
end
