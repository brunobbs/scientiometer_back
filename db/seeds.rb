# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
################################################################################
# Keep
################################################################################
## PQ level or foundation role ###
pq_levels_roles = [
  'I',
  'II',
  'III',
  'IV',
  'V',
  'VI',
  'Analista administrativo',
  'Assistente administrativo',
  'Assistente de pesquisa I',
  'Bioinformata',
  'Biotecnologista',
  'Colaborador',
  'Coordenador',
  'Coordenador de operações',
  'Coordenador de produção',
  'Coordenador de qualidade',
  'Diretor',
  'Gerente de P&D',
  'Gestor de projetos',
  'Liderança científica',
  'Supervisor de produção',
  'Tecnologista de laboratório de desenvolvimento I',
  'Tecnologista de laboratório de desenvolvimento II',
  'Tecnologista de laboratório de desenvolvimento III',
  'Analista administrativo junior',
  'Analista administrativo pleno',
  'Analista administrativo senior',
  'Analista de desenvolvimento senior',
  'Analista de farmacovigilância junior',
  'Analista de farmacovigilância pleno',
  'Analista de farmacovigilância senior',
  'Analista de laboratório de desenvolvimento I',
  'Analista de laboratório de desenvolvimento II',
  'Analista de processos',
  'Analista de sistemas junior',
  'Analista de sistemas pleno',
  'Analista de sistemas senior',
  'Assistente administrativo I',
  'Assistente administrativo II',
  'Assistente administrativo III',
  'Assistente executivo',
  'Auxiliar administrativo',
  'Auxiliar de produção',
  'Auxiliar de limpeza',
  'Auxiliar de laboratório I',
  'Biólogo',
  'Coordenador',
  'Coordenador de banco de dados',
  'Coordenador de farmacovigilância',
  'Coordenador médico de segurança do produto',
  'Coordenador de operações',
  'Coordenador de produção',
  'Coordenador de qualidade',
  'Diretor',
  'Educador',
  'Engenheiro químico e biotecnologista',
  'Especialista de laboratório',
  'Gerente de P&D',
  'Gestor de projetos',
  'Jovem Aprendiz',
  'Médico Veterinário',
  'Monitor de ensaios clínicos',
  'Secretária bilíngue',
  'Servente',
  'Supervisor',
  'Supervisor de farmacovigilância',
  'Supervisor de produção',
  'Técnico de apoio à pesquisa cient. e tecnológica',
  'Técnico de laboratório I',
  'Técnico de microscopia',
  'Técnico de produção I',
  'Técnico de produção II',
  'Técnico de produção III',
  'Técnico em educação',
  'Tecnologista de laboratório de desenvolvimento I',
  'Tecnologista de laboratório de desenvolvimento II',
  'Tecnologista de laboratório de desenvolvimento III',
  'Tecnologista de produção I',
  'Tecnologista de produção II',
  'Tecnologista de produção III',
  'Agente de apoio à pesquisa cient. e tecnológica I',
  'Agente de apoio à pesquisa cient. e tecnológica II',
  'Agente de apoio à pesquisa cient. e tecnológica III',
  'Agente de apoio à pesquisa cient. e tecnológica IV',
  'Agente de saúde',
  'Assistente técnico de apoio à pesquisa cient. e tecnológica I',
  'Assistente técnico de apoio à pesquisa cient. e tecnológica II',
  'Assistente técnico de apoio à pesquisa cient. e tecnológica III',
  'Assistente técnico de apoio à pesquisa cient. e tecnológica IV',
  'Assistente técnico de apoio à pesquisa cient. e tecnológica V',
  'Assistente técnico de apoio à pesquisa cient. e tecnológica VI',
  'Auxiliar administrativo',
  'Auxiliar de apoio à pesquisa cient. e tecnológica I',
  'Auxiliar de apoio à pesquisa cient. e tecnológica II',
  'Auxiliar de laboratório',
  'Auxiliar de saúde',
  'Auxiliar de serviços',
  'Auxiliar de serviços gerais',
  'Chefe I',
  'Oficial administrativo',
  'Oficial de apoio à pesquisa cient. e tecnológica I',
  'Oficial de apoio à pesquisa cient. e tecnológica II',
  'Oficial de apoio à pesquisa cient. e tecnológica III',
  'Oficial de apoio à pesquisa cient. e tecnológica IV',
  'Oficial de serviços gerais',
  'Oficial operacional',
  'Técnico de apoio à pesquisa cient. e tecnológica I',
  'Técnico de apoio à pesquisa cient. e tecnológica II',
  'Técnico de apoio à pesquisa cient. e tecnológica III',
  'Técnico de apoio à pesquisa cient. e tecnológica IV',
  'Técnico de laboratório ',
  'Outro'
]

pq_levels_roles.each do |role|
  RoleFoundationLevel.create(description: role)
end

## Grant agencies
agencies = [
  'American Bird Conservancy',
  'BIOTICK',
  'BNDES',
  'CAPES',
  'CNPq',
  'Conciencias',
  'CONCITEC',
  'Convênio',
  'FAPESP',
  'FAPESP/GSK',
  'FUNDAP',
  'FUSP',
  'Fundação Butantan',
  'Fundação Butantan/Convênio',
  'Intl. Assoc. Study of Pain',
  'IBU/Cellavita',
  'INAGBE',
  'GSK',
  'PAP',
  'Serrapilheira',
  'SESBA',
  'Outra'
]

agencies.each do |agency|
  FundingAgency.create(name: agency)
end

## Instituions
institutions = [
  'IBU',
  'IBU/IPT/USP',
  'FMU',
  'EP-USP',
  'FCF-USP',
  'FM-USP',
  'FMRP-USP',
  'FMVZ-USP',
  'HA Oswaldo Cruz',
  'IB-USP',
  'ICB-USP',
  'IME-USP',
  'IMT-USP',
  'IP-USP',
  'IQ-USP',
  'SSESP',
  'UESC',
  'UFABC',
  'UFRJ',
  'UFSC',
  'UFSM',
  'UFT',
  'UNESP',
  'UNICAMP',
  'UNIFESP',
  'UNIP',
  'UNISA',
  'Uni Anhanguera',
  'USP',
  'Outra'
]

institutions.each do |inst|
  Institution.create(name: inst)
end

####

titles = ['Doutorado', 'Mestrado', 'Graduação', 'Técnico', 'Nível Médio']
titles.each do |title|
  Title.create(name: title)
end

####

degrees = ['Iniciação Científica', 'Mestrado', 'Doutorado', 'Pós-doutorado', 'Aperfeiçoeamento', 'TT1', 'TT2', 'TT3', 'TT4', 'TT5']
degrees.each do |degree|
  AdvisementDegree.create(degree: degree)
end

######

cnpq_levels = %w[
  1A
  1B
  1C
  1D
  2
  SR
]

cnpq_levels.each do |level|
  CnpqLevel.create(level: level)
end

####

qualis = %w[
  A1
  A2
  B1
  B2
  B3
  B4
  B5
  C
]

qualis.each do |level|
  Qualis.create(qualis: level)
end

######

PostDocType.create([{ postdoc_type: 'Nacional' }, { postdoc_type: 'No Exterior' }, { postdoc_type: 'Não Há' }])

######

CollaborationType.create([{ collaboration: 'Nacional' }, { collaboration: 'Internacional' }])

######

LineOfResearch.create([{ name: 'Básica' }, { name: 'Aplicada' }, { name: 'Translacional' }])

######

roles = [
  'Palestra Magna',
  'Comunicação Oral',
  'Poster',
  'Coordenação de Seção',
  'Mesa Redonda',
  'Coordeonação do Evento'
]

roles.each do |role|
  CongressRole.create(role: role)
end

#####
postgraduate_programs = [
  'Anatomia dos Animais Domésticos e Silvestres',
  'Biodiversidade Animal',
  'Biologia Animal',
  'Biologia da Relação Patógeno-Hospedeiro',
  'Biologia Estrutural e Funcional',
  'Biologia Molecular',
  'Bioquímica',
  'Ciências',
  'Ciências Biológicas',
  'Ciências Médicas',
  'Ciências da Computação',
  'Departamento de Pediatria',
  'Farmácia',
  'Farmacologia',
  'Fisiologia Humana',
  'Fisiopatologia Experimental',
  'Genética e Biologia Evolutiva',
  'Imunologia',
  'Interunidades em Bioinformática',
  'Interunidades em Biotecnologia',
  'Medicina e Bem-estar Animal',
  'Medicina Tropical',
  'Medicina Tropical',
  'Medicina Veterinária',
  'Mestrado Profissional em Química e Bioquímica',
  'Microbiologia',
  'Microbiologia e Imunologia',
  'Neurociência e Comportamento',
  'Patologia Experimental e Comparada',
  'Pesquisas Laboratoriais em Saúde Pública',
  'Saúde Baseada em Evidências',
  'Toxinologia',
  'Zoologia',
  'Zoologia',
  'Outro'
].freeze

postgraduate_programs.each do |prog|
  PostgraduateProgram.create(name: prog)
end

laboratories = [
  'Laboratório de Artrópodes',
  'Laboratório de Bacteriologia',
  'Laboratório de Biologia Celular',
  'Laboratório de Bioquímica e Biofísica',
  'Laboratório de Ciclo Celular',
  'Laboratório de Coleções Zoológicas',
  'Laboratório de Desenvolvimento de Vacinas',
  'Laboratório de Ecologia e Evolução',
  'Laboratório de Farmacologia',
  'Laboratório de Fisiopatologia',
  'Laboratório de Herpetologia',
  'Laboratório de Imunogenética',
  'Laboratório de Imunopatologia',
  'Laboratório de Parasitologia',
  'Laboratório de Toxinologia Aplicada'
]
div = LaboratoryDivision.create(name: 'Centro de Desenvolviemto Científico')
laboratories.each do |lab|
  Laboratory.create(name: lab, laboratory_division: div)
end

#####

GrantParticipationType.create([{ name: 'Coordenador' }, { name: 'Pesquisador Principal' }, { name: 'Pesquisador Associado' }])

#####

GrantProjectType.create([{ name: 'Individual' }, { name: 'Temático' }])

#####

ActivityType.create([{ name: 'Instituconal' }, { name: 'Cultural' }, { name: 'Inovação' }, { name: 'Translacional' }, { name: 'Prestação de Serviço' }])

#####

CourseClassification.create([{ classification: 'Difusão' }, { classification: 'Extensão Universitária' }])

####

CourseDegree.create([{ degree: 'Mestrado' }, { degree: 'Doutorado' }])

####

CoordinationDegree.create([{ degree: 'Público Geral' }, { degree: 'Graduandos' }, { degree: 'Pós-Graduandos' }])

####

Profile.create(name: 'Pesquisador', description: 'pesquisador', access_level: '1')
Profile.create(name: 'Diretor de Laboratório', description: 'diretor de laboratório', access_level: '2')
Profile.create(name: 'Diretor de Divisão', description: 'diretor de divisão', access_level: '3')
Profile.create(name: 'Administrador', description: 'administrados de dados do sistema', access_level: '4')
