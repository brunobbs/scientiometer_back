class MultiuserActivitiesController < ApplicationController
  before_action :set_multiuser_sctivity, only: [:show, :update, :destroy]

  # GET /multiuser_activities
  def index
    @multiuser_activities = MultiuserSctivity.all

    render json: @multiuser_activities
  end

  # GET /multiuser_activities/1
  def show
    render json: @multiuser_sctivity
  end

  # POST /multiuser_activities
  def create
    @multiuser_sctivity = MultiuserSctivity.new(multiuser_sctivity_params)

    if @multiuser_sctivity.save
      render json: @multiuser_sctivity, status: :created, location: @multiuser_sctivity
    else
      render json: @multiuser_sctivity.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /multiuser_activities/1
  def update
    if @multiuser_sctivity.update(multiuser_sctivity_params)
      render json: @multiuser_sctivity
    else
      render json: @multiuser_sctivity.errors, status: :unprocessable_entity
    end
  end

  # DELETE /multiuser_activities/1
  def destroy
    @multiuser_sctivity.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_multiuser_sctivity
      @multiuser_sctivity = MultiuserSctivity.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def multiuser_sctivity_params
      params.permit(:activity_id, :multiuser_id, :year)
    end
end
