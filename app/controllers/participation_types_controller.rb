class ParticipationTypesController < ApplicationController
  before_action :set_participation_type, only: [:show, :update, :destroy]

  # GET /participation_types
  def index
    @participation_types = ParticipationType.all

    render json: @participation_types
  end

  # GET /participation_types/1
  def show
    render json: @participation_type
  end

  # POST /participation_types
  def create
    @participation_type = ParticipationType.new(participation_type_params)

    if @participation_type.save
      render json: @participation_type, status: :created, location: @participation_type
    else
      render json: @participation_type.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /participation_types/1
  def update
    if @participation_type.update(participation_type_params)
      render json: @participation_type
    else
      render json: @participation_type.errors, status: :unprocessable_entity
    end
  end

  # DELETE /participation_types/1
  def destroy
    @participation_type.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_participation_type
      @participation_type = ParticipationType.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def participation_type_params
      params.require(:participation_type).permit(:first_author, :last_author, :corresponding_author)
    end
end
