# README

* System dependencies
  * Ruby
  * Rails

* Configuration
  * `bundle install` to install all gems needed

* Database creation
 * `rails db:create` to create a database if not already exists. NOTE: the DB must be up and running for this.

* Database initialization
  * `rails db:migrate` to run all db migrations


* How to run the test suite
  * `rails test`

* Deployment instructions
  * `rails s` to start the server



